using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ScreenShot : MonoBehaviour
{
    public GameObject Panel;

    public void open(){
        if(Panel != null){
            bool isActive = Panel.activeSelf;
            Panel.SetActive(!isActive);
        }
    }

    public void ClickShare(){
	
      StartCoroutine(TakeScreenshotAndShare());
    }
	// API KEY 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
    
    private IEnumerator TakeScreenshotAndShare()
{
	yield return new WaitForEndOfFrame();

	Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
	ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
	ss.Apply();

	string filePath = Path.Combine( Application.temporaryCachePath, "image.png" );
	File.WriteAllBytes( filePath, ss.EncodeToPNG() );

	// To avoid memory leaks
	Destroy( ss );

	new NativeShare().AddFile( filePath )
		.SetSubject( "Mon mod�le 3D!!!" )
		.Share();

	
		Panel.SetActive(false);


}

	public void Cancel(){
		Panel.SetActive(false);
	}
}
